# Images
ghost:5.8.2-alpine with SQLITE and S3 (prior to MYSQL move):
`registry.gitlab.com/theplacelab/image/ghost-with-s3:a1499098`

# Ghost with S3 and Backup

Ghost blogging platform with S3 support and auto-backup baked in.

- Based on latest `ghost:alpine` image.

- Backups: Daily cronjob compresses, encrypts and uploads backup file to S3

## Backups

### Configuring cron

- Edit `backup/crontab.txt` to adjust the timing

### Cheatsheet:

[Cron Guru](https://crontab.guru/) because who can remember this...

```
# Daily at 2am: "0 2 \* \* \*"
# Every 5 min: "_/5 _ \* \* \*"
# Sun at Midnight: 0 0 * * 0
```

### Restoring backups

- Retrieve the `FILENAME.gpg` file from S3

- Unencrypt using the password you specified:  
  `gpg --output NAME.tar.bz2 -d NAME.tar.bz2.gpg`

- Update PVC via a running container:  
  `kubectl cp PATH_TO_CONTENT/ POD:/var/lib/ghost --namespace NAMESPACE`

- Restart deployment and you should be good to go

## Environment Vars

Backup specific:
|Var|Note|
|-|-|
|BACKUP_PASSWORD| Used to encrypt backup file|
|BACKUP_MAIL_DOMAIN| Mail domain|
|BACKUP_CONFIRMATION_TO| Where to email backup confirmation, leave blank for no email|

Ghost (these are used by email and s3 also by backup script For full list and details see [Ghost docs](https://ghost.org/docs/config/).):
|Var|Note|
|-|-|
|url|https://example.com|
|mail\_\_transport|SMTP|
|mail\_\_from|mail@example.com|
|mail\_\_service|Mailgun|
|mail\_\_host|smtp.mailgun.org|
|mail\_\_secure|true/false|
|mail\_\_port|587|
|mail\_\_auth\_\_user|postmaster@example.com|
|mail\_\_auth\_\_pass|\*\*\*|
|storage\_\_active|s3|
|storage\_\_s3\_\_accessKeyId|\*\*\*|
|storage\_\_s3\_\_secretAccessKey|\*\*\*|
|storage\_\_s3\_\_region|us-east-1|
|storage\_\_s3\_\_bucket|\*\*\*|
|storage\_\_s3\_\_pathPrefix|\*\*\*|
|storage\_\_s3\_\_endpoint|s3.wasabisys.com|
|storage\_\_s3\_\_assetHost|https://assets.example.com|
