#!/bin/bash

if [[ "$*" == node*current/index.js* ]]; then
    # start cron
    /usr/sbin/crond -f -l 8 &
fi

exec docker-entrypoint.sh "$@"