#!/bin/bash

# Ghost uses lowercase env vars, this prevents shellcheck from complaining
# shellcheck disable=SC2154
S3_PATH="s3://$storage__s3__bucket/$storage__s3__pathPrefix/backups/"
S3_REGION=$storage__s3__region
S3_ENDPOINT=$storage__s3__endpoint
ID="$storage__s3__pathPrefix"

# Back up content dir
backupFile=/tmp/"$ID"_"$(date +%Y%m%d_%H%M%S)".tar.bz2
cd /var/lib/ghost || ( echo "Ghost content missing!" && exit 1 )
tar -cjf "$backupFile" content/

# Encrypt
gpg --batch --passphrase "$BACKUP_PASSWORD" -c "$backupFile"
encryptedFile="$backupFile.gpg"
LOG=$(ls -la "$encryptedFile")
rm "$backupFile"

# Send to S3
export AWS_SECRET_ACCESS_KEY="$storage__s3__secretAccessKey" 
export AWS_ACCESS_KEY_ID="$storage__s3__accessKeyId"
aws s3 cp "$encryptedFile" "$S3_PATH" --region="$S3_REGION" --endpoint-url="https://$S3_ENDPOINT" 
rm "$encryptedFile"

# Notify
if [ -z "$BACKUP_CONFIRMATION_TO" ]; then
    exit 1;
else
    conf=$(
        echo "root=postmaster"
        echo "mailhub=smtp.mailgun.org:587"
        echo "FromLineOverride=YES"
        echo "rewriteDomain=$BACKUP_MAIL_DOMAIN"
        echo "AuthUser=$mail__options__auth__user"
        echo "AuthPass=$mail__options__auth__pass"
        echo "UseSTARTTLS=YES"
        echo "hostname=$BACKUP_MAIL_DOMAIN"
    )
    mkdir -p /etc/ssmtp/
    echo "$conf" > /etc/ssmtp/ssmtp.conf
    email=$(
        printf "%s\n"   "To: $BACKUP_CONFIRMATION_TO"
        printf "%s\n"   "From: ghostbackup@$BACKUP_MAIL_DOMAIN"
        printf "%s\n\n" "Subject: [$ID] Ghost is backed up!"
        printf "%s\n"   "$LOG"
    )
    ssmtp -t <<< "$email"
fi
