FROM ghost:alpine
WORKDIR /var/lib/ghost
RUN npm install ghost-storage-adapter-s3 \
    && mkdir -p ./content.orig/adapters/storage \
    && cp -vr ./node_modules/ghost-storage-adapter-s3 ./content.orig/adapters/storage/s3

# Various support tools
RUN apk update \
    && apk add gnupg \
    && apk add bzip2 \
    && apk add ssmtp \
    && apk add apk-cron \
    && rm -rf /var/cache/apk/*

# AWS CLI
RUN apk add --no-cache \
    python3 \
    py3-pip \
    pipx \
    && pipx install awscli \
    && rm -rf /var/cache/apk/*

# Cron job (for backup)
# https://stackoverflow.com/questions/37015624/how-to-run-a-cron-job-inside-a-docker-container
COPY crontab.txt /crontab.txt
COPY do-backup.sh /usr/local/bin/do-backup.sh
COPY entry.sh /usr/local/bin/entry.sh
RUN chmod 755 /usr/local/bin/do-backup.sh /usr/local/bin/entry.sh
RUN /usr/bin/crontab /crontab.txt

ENTRYPOINT ["entry.sh"]
CMD ["node", "current/index.js"]
